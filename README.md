# Welcome !
Hi, I'm Alexis, a computer science student looking to become a video game developer. 

You will find here my games created with [Godot Engine](https://godotengine.org/).

### B-Plan(e)
B-Plan(e) is a 2d aircraft combat game where you can take control of 4 different aircraft against AI or in multiplayer.
The game is in Beta, so it is possible to encounter bugs or non implemented features. 
### 8Pixel
8Pixel is a small puzzle game where the goal is to put pieces of different shapes on a grid, being careful not to destroy a square by overloading it. The different squares take shades of white, yellow, orange, red and finally black according to their life. When placing a piece, a square is drawn in green if placing the piece will not destroy the square, in cyan if the square will be destroyed and in blue if it is not possible to place the piece.

**Controls**

 - Directional arrows for moving a piece
 - Space to turn a piece
 - Enter to put a piece
 - Escape to pause or resume
 
The game is in Beta, so it is possible to encounter bugs or non implemented features. 
### Copyright
My games use royalty-free music, created by myself or available [here](https://www.youtube.com/c/kmmusic/videos).
The sound effects come from the [BBC website](http://bbcsfx.acropolis.org.uk/) ([License](https://github.com/bbcarchdev/Remarc/blob/master/doc/2016.09.27_RemArc_Content%20licence_Terms%20of%20Use_final.pdf))

You can download my games for free but you are not allowed to reproduce, modify or republish them.